import numpy.testing as npt


def test_exercise_0(x, y, z):
    assert type(x) == int, "x is not int type"
    assert type(y) == float, "y is not float type"
    assert type(z) == float, "z is not float type"
    assert z == x * y, "z is not equal to x * y"
    print("All basic tests passed! Good job!")

def test_exercise_1(first_name, last_name, birth_year, origin_city, origin_country):
    assert type(first_name) == str, "first_name is not str type"
    assert type(last_name) == str, "last_name is not str type"
    assert type(birth_year) == int, "birth_year is not int type"
    assert type(origin_city) == str, "origin_city is not str type"
    assert type(origin_country) == str, "origin_country is not str type"
    assert len(first_name) > 0, "first_name is empty str"
    assert len(last_name) > 0, "last_name is empty str"
    assert len(origin_city) > 0, "origin_city is empty str"
    assert len(origin_country) > 0, "origin_country is empty str"
    print("All basic tests passed! Good job!")

def test_exercise_2(current_year, birth_year, age):
    assert type(current_year) == int, "current_year is not int type"
    assert type(birth_year) == int, "birth_year is not int type"
    assert type(age) == int, "age is not int type"
    assert age == (current_year - birth_year), "incorrect age calculation"
    print("All basic tests passed! Good job!")


def test_exercise_3(age, age_in_days):
    assert type(age) == int, "age is not int type"
    assert type(age_in_days) == int, "age_in_days is not int type"
    total_days = age * 365
    assert age_in_days == total_days, "incorrect age in days calculation"
    print("All basic tests passed! Good job!")

def test_exercise_4(age, first_name, greeting):
    assert type(age) == int, "age is not int type"
    assert type(first_name) == str, "first_name is not str type"
    assert (
        greeting == f"Hi! My name is {first_name} and I'm {age} years old"
    ), "greeting string is wrongly formatted. ensure there are no extra spaces"
    print("All basic tests passed! Good job!")

def test_exercise_5(first_name, last_name, full_name):
    assert type(first_name) == str, "first_name is not str type"
    assert type(last_name) == str, "last_name is not str type"
    assert type(full_name) == str, "full_name is not str type"
    assert (
        full_name == f"{first_name} {last_name}"
    ), "full_name does not equal to concatenation of first_name and last_name"
    print("All basic tests passed! Good job!")

def test_exercise_6(first_name, last_name, len_full_name):
    assert type(first_name) == str, "first_name is not str type"
    assert type(last_name) == str, "last_name is not str type"
    assert type(len_full_name) == int, "full_name is not int"
    first_name_no_spaces = first_name.replace(" ", "")
    last_name_no_spaces = last_name.replace(" ", "")
    assert len_full_name == (
        len(first_name_no_spaces) + len(last_name_no_spaces)
    ), "incorrect amount of characters. maybe you are considering spaces?"
    print("All basic tests passed! Good job!")

def test_exercise_7(meters, inches, feet):
    assert type(meters) == float, "meters should be float type"
    assert type(inches) == float, "inches should be float type"
    assert type(feet) == float, "feet should be float type"
    npt.assert_almost_equal(inches, (meters * 39.37), decimal=5, err_msg='incorrect inches')
    npt.assert_almost_equal(feet, (meters * 3.28), decimal=5, err_msg='incorrect feet')
    print("All basic tests passed! Good job!")

def test_exercise_8_1(stock_1, stock_2, stock_3, SENOVA):
    assert stock_1[0] in SENOVA.keys(), "stock_1 is not part of SENOVA"
    assert SENOVA[stock_1[0]]==stock_1[1], "stock_1 is listed in SENOVA, but has wrong price."
    assert stock_2[0] in SENOVA.keys(), "stock_1 is not part of SENOVA"
    assert SENOVA[stock_2[0]]==stock_2[1], "stock_2 is listed in SENOVA but has wrong price."
    assert stock_3[0] in SENOVA.keys(), "stock_3 is not part of SENOVA"
    assert SENOVA[stock_3[0]]==stock_3[1], "stock_3 is listed in SENOVAV but has wrong price."
    print('All basic tests passed! Good job!')

def test_exercise_8_2(stock_1, stock_2, stock_3, stocks):
    assert stock_1 in stocks, "stock_1 is not in stocks list"
    assert stock_2 in stocks, "stock_2 is not in stocks list"
    assert stock_3 in stocks, "stock_3 is not in stocks list"
    print('All basic tests passed! Good job!')

def test_exercise_8_3(stocks):
    assert type(stocks)==list, "stocks is not list type"
    assert all([type(i)==list for i in stocks]), "stocks list item is not list type"
    assert all([len(i) == 3 for i in stocks]), "stocks list item has less than 3 elements (did you add quantitiy?)"
    assert all([type(i[2])==int for i in stocks]), "stock quantity is not int type"
    assert stocks[0][2] == 1, "you are supposed to have 1 stock of stock_1"
    assert stocks[1][2] == 2, "you are supposed to have 2 stocks of stock_2"
    assert stocks[2][2] == 3, "you are supposed to have 3 stocks of stock_3"
    print('All basic tests passed! Good job!')

def test_exercise_8_4(stocks, total_stocks, total_price, stock_buy_order):
    assert type(stock_buy_order) == str, "stock_buy_order is not str type"
    assert (
        total_stocks == stocks[0][2] + stocks[1][2] + stocks[2][2]
    ), "Wrong nbr of total stocks"
    assert total_price == (1 * stocks[0][1]) + (2 * stocks[1][1]) + (
        3 * stocks[2][1]
    ), "Wrong total price"
    expected_string = f"I want to buy a total of {total_stocks} stocks. I want {stocks[0][2]} stock of {stocks[0][0]}, {stocks[1][2]} stocks of {stocks[1][0]} and {stocks[2][2]} stocks of {stocks[2][0]}, for a total price of {total_price}€"
    expected_string_clean = (
        expected_string.rstrip().lstrip().replace(" ", "").replace("\n", "").lower()
    )
    stock_buy_order_clean = (
        stock_buy_order.rstrip().lstrip().replace(" ", "").replace("\n", "").lower()
    )
    print("Expected:")
    print(expected_string)
    print("Your solution:")
    print(stock_buy_order.lstrip())
    assert (
        expected_string_clean == stock_buy_order_clean
    ), "stock_buy_order has wrong format, ensure you use the template as described in exercise"
    print("All basic tests passed! Good job!")

def test_exercise_8_5(stocks_dict, stocks):
    assert 'stock_1' in stocks_dict, "stock_1 is not in stocks_dict"
    assert 'stock_2' in stocks_dict, "stock_2 is not in stocks_dict"
    assert 'stock_3' in stocks_dict, "stock_3 is not in stocks_dict"
    assert type(stocks_dict['stock_1']) == dict, "stock_1 is not dict type"
    assert type(stocks_dict['stock_2']) == dict, "stock_2 is not dict type"
    assert type(stocks_dict['stock_3']) == dict, "stock_3 is not dict type"
    for key in stocks_dict.keys():
        for subkey in ['stock_abb', 'stock_price', 'stock_quantity']:
            assert subkey in stocks_dict[key], f"{subkey} subkey missing in key {key}"
            
    for idx, key in enumerate(stocks_dict.keys()):
        assert stocks_dict[key]['stock_abb'] == stocks[idx][0], f"{key} has wrong stock_abb"
        assert stocks_dict[key]['stock_price'] == stocks[idx][1], f"{key} has wrong stock_price"
        assert stocks_dict[key]['stock_quantity'] == stocks[idx][2], f"{key} has wrong stock_quantity"     
    print('All basic tests passed! Good job!')

def test_exercise_9_1(fausto_age, pisco_age, plutao_age, average_age):
    assert fausto_age == 3, "fausto age is wrong"
    assert pisco_age == 11, "pisco age is wrong"
    assert plutao_age == 1, "plutao age is wrong"
    assert average_age == 5, "average age is wrong"
    print('All basic tests passed! Good job!')

def test_exercise_9_2(nr_dogs):
    assert nr_dogs == 3, "Wrong Number"
    print('All basic tests passed! Good job!')
